provider "aws" {
     region = "ap-south-1"
}

resource "aws_instance" "r1" {
  ami           = "ami-0d758c1134823146a"
  instance_type = "t2.micro"
  key_name="thiyagu"
  count = 2
  tags = {
            Name="${count.index == 0 ? "MyMachine1" : "MyMachine${count.index}"+1}"
}

connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = file("thiyagu.pem")
    host     = self.public_ip
  }

}


resource "aws_s3_bucket"  "s3" {
       bucket = "thiyamars"
}
